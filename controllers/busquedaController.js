const busquedaService = require('../services/busquedaService');

//FUNCIONA
module.exports.findById = async function(req, res) {
    const responseObj = { status: 500, message: 'Internal server error' };
    try {
        const busquedaId = req.params.id;
        const responseFromService = await busquedaService.findById(busquedaId);
        if (responseFromService.status) {
            if (responseFromService.result) {
                responseObj.body = responseFromService.result;
                responseObj.message = 'Busqueda fetched successfully';
                responseObj.status = 200;
            } else {
                responseObj.message = 'Busqueda not found';
                responseObj.status = 404;
            }
        }
    } catch(error) {
        console.log('ERROR-busquedaController-findById: ', error);
    }
    return res.status(responseObj.status).send(responseObj);
}

//FUNCIONA
module.exports.findAll = async function(req, res) {
    const responseObj = { status: 500, message: 'Internal server error' };
    try {
        const responseFromService = await busquedaService.findAll();
        if (responseFromService.status) {
            if (responseFromService.result) {
                responseObj.body = responseFromService.result;
                responseObj.message = 'Busqueda fetched successfully';
                responseObj.status = 200;
            } else {
                responseObj.message = 'No busqueda found';
                responseObj.status = 404;
            }
        }
    } catch(error) {
        console.log('ERROR-busquedaController-findAll: ', error);
    }
    return res.status(responseObj.status).send(responseObj);
}

//funciona
module.exports.create = async function(req, res) {
    const responseObj = { status: 500, message: 'Internal server error' };
    try {
        const data = req.body ;
        const responseFromService = await busquedaService.create(data);
        if (responseFromService.status) {
            responseObj.body = responseFromService.result;
            responseObj.message = 'Busqueda created successfully';
            responseObj.status = 201;
        }
    } catch(error) {
        console.log('ERROR-busquedaController-create: ', error);
    }
    return res.status(responseObj.status).send(responseObj);
}

module.exports.update = async function(req, res) {
    let responseObj = { status: 500, message: 'Internal server error' };
    try {
        const busqueda = req.body;
        busqueda.id = req.params.id;
        const responseFromService = await busquedaService.update(busqueda);
        if (responseFromService.status) {
            responseObj.body = responseFromService.result;
            responseObj.message = 'Busqueda updated successfully';
            responseObj.status = 200;
        }
    } catch(error) {
        console.log('ERROR-busquedaController-update: ', error);
    }
    return res.status(responseObj.status).send(responseObj);
}

module.exports.delete = async function(req, res) {
    let responseObj = { status: 500, message: 'Internal server error' };
    try {
        const busquedaId = req.params.id;
        const responseFromService = await busquedaService.delete(busquedaId);
        if (responseFromService.status) {
            responseObj.body = responseFromService.result;
            responseObj.message = 'Busqueda removed successfully';
            responseObj.status = 200;
        }
    } catch(error) {
        console.log('ERROR-busquedaController-delete: ', error);
    }
    return res.status(responseObj.status).send(responseObj);
}