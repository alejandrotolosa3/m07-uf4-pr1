const mongoose = require('mongoose');
const Busqueda = require('../models/db/busquedaModel');
const crudRepository = require('../database/crudRepository');


//FUNCIONA
module.exports.findById = async function(busquedaId) {
    const responseObj = { status: false };
    try {
        const data = {
            findQuery: {_id: mongoose.Types.ObjectId(busquedaId)},
            model: Busqueda,
            projection: { __v: false }
        };
        const responseFromRepository = await crudRepository.findById(data);
        if (responseFromRepository.status) {
            responseObj.status = true;
            responseObj.result = responseFromRepository.result;
        }
    } catch (error){
        console.log('ERROR-busquedaService-findById: ', error);
    }
    return responseObj;
}

//FUNCIONA
module.exports.findAll = async function() {
    const responseObj = { status: false };
    try {
        const data = {
            findQuery: {},
            model: Busqueda,
            projection: {
                __v: false
            }
        };

        const responseFromDatabase = await crudRepository.find(data);
        if (responseFromDatabase.status) {
            responseObj.status = true;
            responseObj.result = responseFromDatabase.result;
        }
    } catch (error){
        console.log('ERROR-busquedaService-findAll: ', error);
    }
    return responseObj;
}

module.exports.create = async function(dataFromController) {
    const responseObj = { status: false };
    try {
        const busqueda = new Busqueda(dataFromController);
        const responseFromDatabase = await crudRepository.save(busqueda);
        if (responseFromDatabase.status) {
            responseObj.status = true;
            responseObj.result = responseFromDatabase.result;
        }
    } catch (error){
        console.log('ERROR-busquedaService-create: ', error);
    }
    return responseObj;
}

module.exports.update = async function(busqueda) {
    const responseObj = { status: false };
    try {
        const data = {
            findQuery: {
                _id: mongoose.Types.ObjectId(busqueda.id)
            },
            model: Busqueda,
            projection: {
                __v: false
            },
            updateQuery: {}
        };

        if (busqueda.fecha_hora) data.updateQuery.fecha_hora = busqueda.fecha_hora;


        const responseFromDatabase = await crudRepository.findOneAndUpdate(data);
        if (responseFromDatabase.status) {
            responseObj.status = true;
            responseObj.result = responseFromDatabase.result;
        }
    } catch (e){
        console.log('ERROR-busquedaService-update: ', e);
    }
    return responseObj;
}

module.exports.delete = async function(busquedaId) {
    const responseObj = { status: false };
    try {
        const data = {
            findQuery: {
                _id: mongoose.Types.ObjectId(busquedaId)
            },
            model: Busqueda,
            projection: {
                __v: false
            }
        };

        const responseFromDatabase = await crudRepository.findOneAndDelete(data);
        if (responseFromDatabase.status) {
            responseObj.status = true;
            responseObj.result = responseFromDatabase.result;
        }
    } catch (error){
        console.log('ERROR-busquedaService-delete: ', error);
    }
    return responseObj;
}