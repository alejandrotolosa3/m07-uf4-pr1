const express = require('express');
const router = express.Router();
const busquedaController = require('../controllers/busquedaController');
const joiSchemaValidation = require('../middlewares/joiSchemaValidation');
const busquedaSchemas = require('../models/joi/busquedaSchemas');
//const tokenValidation = require('../middlewares/tokenValidation');


router.get('/list',
     //tokenValidation.validate,
    busquedaController.findAll
    );

router.get('/list/:id',
    //tokenValidation.validate,
    busquedaController.findById
);

router.delete('/delete/:id',
    //tokenValidation.validate,
    busquedaController.delete
);

router.post('/create',
    //tokenValidation.validate,
    busquedaController.create
);

router.put('/update/:id',
    //tokenValidation.validate,
    busquedaController.update
);




    module.exports = router;



























// router.get('/details/:id?',
//     tokenValidation.validate,
//     //joiSchemaValidation.validate(userSchemas.userIdSchema, 'path'),
//     userController.findById);

// router.get('/list',
//     joiSchemaValidation.validate(userSchemas.getUserListSchema, 'query'),
//     userController.findAll);

// router.post('/create', 
//     joiSchemaValidation.validate(userSchemas.createUserSchema, 'body'),
//     userController.create);

// router.put('/update/:id',
//     joiSchemaValidation.validate(userSchemas.userIdSchema, 'path'),
//     joiSchemaValidation.validate(userSchemas.updateUserSchema, 'body'),
//     userController.update);

// router.delete('/delete/:id',
//     joiSchemaValidation.validate(userSchemas.userIdSchema, 'path'),
//     userController.delete);

 module.exports = router;